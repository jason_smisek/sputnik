# Sputnik

## CKP 101

Please contact Kato-Developers@centene.com for any questions

| Software Requirements| Version|
| ------------- |-------------|
| Docker | 19.03.5, build 633a0ea |
| Docker-Compose | 1.25.4, build 8d51620a |
| kubectl | v1.15.5 |
| helm | v3.1.1 |
| curl | any version |

## DOCKER
    Open Powershell, cmd, terminal, or whatever command shell you want
    
    cd to where you want to place the folder

#### Clone Repo:
    git clone https://gitlab.centene.com/kato/sputnik.git
    cd sputnik

#### BUILD Docker image
    cd docker
    docker build -t hello-world:v1 -f Dockerfile .

#### Run Container
    docker run -d -p 6000:8080 hello-world:v1

#### Verify Container is running
    docker ps
    curl http://localhost:6000

#### Push docker image to artifactory
    docker login artifactory-rco.centene.com
    docker tag hello-world:v1 artifactory-rco.centene.com/kato_docker_non-production_local_rco/hello_world:1.0.0
    docker push artifactory-rco.centene.com/kato_docker_non-production_local_rco/hello_world:1.0.0

## KUBERNETES

Login to the CKP rancher discovery namespace:

https://rancher.ckp-discovery.centene.com

Copy kube config into ~/.kube/config

Windows users, it will be C:\users\<username>\.kube

#### Deploy APP
    cd ..
    cd kubernetes
    kubectl apply -f service.yaml -f deployment.yaml -f ingress.yaml --namespace kato

#### Verify
    kubectl get pods --namespace kato

#### To Delete
    kubectl delete --namespace=kato -f service.yaml -f ingress.yaml -f deployment.yaml

## HELM CHARTS

#### Deploy helm chart 
    cd ..
    cd helm/hello-world
    helm install hello-world -f values.yaml --namespace kato --version 1.0.0 --debug --kubeconfig ~/.kube/config
    
    For Windows Machines type the following:
    cd ..
    cd helm\hello-world
    helm install . -f values.yaml --namespace=kato --tiller-namespace=kato-tiller  --version 1.0.0 --debug --kubeconfig C:\users\<username>\.kube\config

#### Verify
    curl -k https://kato-hello-world-helm.ckp-discovery.centene.com

#### List helm charts
    helm list --namespace kato

#### Test
    helm test hello-world --namespace kato

#### Delete
    helm delete --purge hello-world

